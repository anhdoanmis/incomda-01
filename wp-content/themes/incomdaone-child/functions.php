<?php
/**
 * Child theme functions
 *
 * @package Incomda
 */

/**
 * Load the parent style.css file
 *
 * @link http://codex.wordpress.org/Child_Themes
 */
function incomda_child_scripts() {
	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme).
	$theme   = wp_get_theme( 'incomdaone' );
	$version = $theme->get( 'Version' );
	// Load the stylesheet.
	wp_enqueue_style( 'incomdaone-style', get_template_directory_uri() . '/style.css', array(), $version );
	wp_enqueue_style( 'incomdaone-child-style', get_stylesheet_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'incomda_child_scripts' );
