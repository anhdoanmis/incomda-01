<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="profile" href="https://gmpg.org/xfn/11"/>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <header id="masthead" class="<?php echo is_singular() && incomda_can_show_post_thumbnail() ? esc_attr('site-header featured-image') : esc_attr('site-header'); ?>">
        <div class="site-branding-container fixed-top <?php echo !is_home() && !is_front_page() ? esc_attr('sticky') : ''; ?>">
            <?php get_template_part('template-parts/header/site', 'branding'); ?>
        </div><!-- .site-branding-container -->
        <?php if (!is_home() && !is_front_page()): ?>
            <div class="site-branding-container " style="visibility: hidden;">
                <?php get_template_part('template-parts/header/site', 'branding'); ?>
            </div><!-- .site-branding-container -->
        <?php endif; ?>
    </header><!-- #masthead -->
    <div id="content" class="site-content">

