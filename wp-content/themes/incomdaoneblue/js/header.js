jQuery(document).ready(function ($) {
    $(window).scroll(function () {
        var offsettop = $('.site-branding-container').height();
        if ($(document).scrollTop() >= offsettop) {
            $(".site-branding-container").addClass("sticky");
        } else {
            $(".site-branding-container").removeClass("sticky");
        }
    });
});