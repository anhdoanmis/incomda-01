<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_incomda_01' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$>{=3F:Ft+23*>2O)t{lh; q*s*N*<l/C[#<.jDo?sO^|EQLfzr[Zm_,}yGO3OXP' );
define( 'SECURE_AUTH_KEY',  'S1`d5oCf?~yY2$q&YV-R2-Bio.~,y~X;6-OxF6Quwjn})j$]d<K/$.BY2N.>RP~{' );
define( 'LOGGED_IN_KEY',    'S?R4S]t!r#$PNV+#_pU:<qWu1vi s0/ketoC_YdP=|@X{vK?1!(c{c,HA+xZ;dwB' );
define( 'NONCE_KEY',        'k!I)C;N&h=Hj@9j|8W$L4abAXb3I:;g{V %#3Z _MBr}DpnLa!6#(_5%cR~:z?N-' );
define( 'AUTH_SALT',        'hc+Gp!?*$eM8jsWEH-@C4:,nEF~:=-Y2wDhPt?^`a_B|2D.p79GaOO5&CpvDV<hw' );
define( 'SECURE_AUTH_SALT', 'i[!iJT:k6 Dx<cmrLnJYPvwaeP}8uPNOWSRY,.5fs5`~^y{(r6Ei}<sA0B*2!>!s' );
define( 'LOGGED_IN_SALT',   'VqOzvj>}?d>54EWgqoQv_q%z:Yus0qQCEFS>1|jv@vH{P4C=J<<$.KLi%fp`md)e' );
define( 'NONCE_SALT',       'qy#P&![i{UB8j1es$1 Mdn-z}6JDbT:c$k+flA?t_+HG.<]]-.73pO!{&;&lC&m3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'nbfp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
error_reporting(E_ALL);
ini_set('display_errors', 1);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
